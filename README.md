Introduction
============

A library capable of loading a tree (a forest, actually) from a file. This tree is meant to be human-readable, with a free (as long as it is consistent) indentation style.

Simtree format
===============

A Simtree must be organized in such a way that every child is one indentation deeper than its parent, and all siblings have the same indentation.
The String used for indentation is inferred from the first indentation depth.
The entire first line (except the newline character) represents the beginning of a comment.

Example file:

	#
	A
		B
		C
	D
		E
			F
				G
	H
