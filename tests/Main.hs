module Main where

import Data.Simtreelo as Simtree
import Data.Tree

loadTest f = do
  tf <- readFile f
  mapM_ (\(x,y) -> putStr (show x ++ ".") >> putStrLn y) $ zip [1,2..] (lines tf)
  let tree = Simtree.loadString tf
  case tree of        
    Left err -> putStrLn err
    Right t -> putStrLn (drawForest t) -- >> print t
  putStrLn "#######"

mergeTest (h:r) = putStrLn $ drawForest $ foldl merge [h] r 

testFiles = ["test1","test2","test3","test4","test5","test6","test","bookmarks"]
mergeTests = ["mergeTest1","mergeTest2","mergeTest3"]

ignoreLeft [] = []
ignoreLeft ((Left _) : r) = ignoreLeft r
ignoreLeft (Right h:r) = h : ignoreLeft r

main = do
  mapM_ loadTest testFiles
  forest <- mapM Simtree.loadFile mergeTests
  putStrLn "Merge test\n#######"
  mergeTest $ concat $ ignoreLeft forest
  
